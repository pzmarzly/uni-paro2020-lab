# PARO assignment

Task 1: implemented basic per-letter calculation. Handled lowercase strings and unknown characters.

Task 2: fixed the code, implemented leap year calculations.

Task 3: written basic scenarios.

Extra: configured CI.
