# User scenarios

GIVEN: The game board is ready
AND The game has not started yet
AND The car piece is available
WHEN: Player chooses to play as a car
THEN: Player becomes a car
AND The car piece is not available

GIVEN: The game has started
AND It's the player's turn
WHEN: The player rolls a dice
THEN: The piece moves by that number

GIVEN: The game has started
AND The player piece is before the start/finish line
WHEN: The player moves onto or over the start/finish line
THEN: The player is awarded some cash

GIVEN: The game has started
AND It's the player's turn
AND The player is in jail
WHEN: The player uses the "Get Out of Jail Free Card"
THEN: The player is not in jail
