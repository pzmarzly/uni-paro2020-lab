#include "scrabble-score.hpp"
#include "gtest/gtest.h"

struct ScrabbleScoreSuite {};

TEST(ScrabbleScoreSuite, EmptyWordScoreIs0) { ASSERT_EQ(scrabbleScore(""), 0); }

TEST(ScrabbleScoreSuite, HelloWordTest) {
    ASSERT_EQ(scrabbleScore("HELLO"), 8);
}

TEST(ScrabbleScoreSuite, LowercaseIsHandled) {
    ASSERT_EQ(scrabbleScore("HeLlO"), 8);
}

TEST(ScrabbleScoreSuite, InvalidCharactersAreIgnored) {
    ASSERT_EQ(scrabbleScore("HeL?l O!"), 8);
}

TEST(ScrabbleScoreSuite, PotatoWordTest) {
    ASSERT_EQ(scrabbleScore("POTATO"), 8);
}

TEST(ScrabbleScoreSuite, CabbageWordTest) {
    ASSERT_EQ(scrabbleScore("Cabbage"), 14);
}

TEST(ScrabbleScoreSuite, TheQuickBrownTest) {
    ASSERT_EQ(scrabbleScore("THEQUICKBROWN"), 36);
}

TEST(ScrabbleScoreSuite, FoxJumpsOverTest) {
    ASSERT_EQ(scrabbleScore("FOXJUMPSOVER"), 36);
}

TEST(ScrabbleScoreSuite, TheLazyDogTest) {
    ASSERT_EQ(scrabbleScore("THELAZYDOG"), 27);
}
