#include "day-of-year.hpp"

int dayOfYear(int month, int dayOfMonth, int year) {
    bool leapYear = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    const int dayInMonths[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    for(int i = 1; i < month; i++) {
        dayOfMonth += dayInMonths[i - 1];
    }
    if(leapYear && month > 2) {
        dayOfMonth += 1;
    }
    return dayOfMonth;
}
