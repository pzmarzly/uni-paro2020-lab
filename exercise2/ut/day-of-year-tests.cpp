#include "day-of-year.hpp"
#include "gtest/gtest.h"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, January1stIsFirstDayOfYear) {
    ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, February1stIs32ndDayOfYear) {
    ASSERT_EQ(dayOfYear(2, 1, 2020), 32);
}

TEST(DayOfYearTestSuite, NonLeapYearIsHandledCorrectly) {
    ASSERT_EQ(dayOfYear(3, 1, 2019), 60);
}

TEST(DayOfYearTestSuite, LeapYearIsHandledCorrectly) {
    ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}

TEST(DayOfYearTestSuite, December31stIsLastDayOfYear) {
    ASSERT_EQ(dayOfYear(12, 31, 2019), 365);
}

TEST(DayOfYearTestSuite, DayInJuneIsHandledCorrectly) {
    ASSERT_EQ(dayOfYear(6, 2, 2020), 154);
}

TEST(DayOfYearTestSuite, DayInNovemberIsHandledCorrectly) {
    ASSERT_EQ(dayOfYear(11, 3, 2020), 308);
}

TEST(DayOfYearTestSuite, NonLeapYear) {
    ASSERT_EQ(dayOfYear(11, 3, 1900), 307);
}

TEST(DayOfYearTestSuite, LeapYearModulo100) {
    ASSERT_EQ(dayOfYear(11, 3, 1900), 307);
}

TEST(DayOfYearTestSuite, LeapYearModulo400) {
    ASSERT_EQ(dayOfYear(11, 3, 2000), 308);
}
